<?php 
	function wp_theme1_plugin_activation(){

		//Declare plugin must install
		$plugins = array(
			array(
				'name' => 'Redux Framework',
				'slug' => 'redux-framework',
				'required' => true
			),
		);

		//Setup TGM

		$configs = array(
			'menu' => 'tp_plugin_install',
			'has_notice' => true,
			'dismissable' => false,
			'is_automatic' => true
		);

		tgmpa($plugins, $configs);

	}

	add_action('tgmpa_register', 'wp_theme1_plugin_activation');
?>
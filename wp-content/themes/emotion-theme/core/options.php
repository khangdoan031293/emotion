<?php 
    
    if(!class_exists('wp_theme1_options')){
        class wp_theme1_options{
            public $args = array();
            public $sections = array();
            public $theme;
            public $ReduxFramework;

            public function __construct() {
 
                 if ( ! class_exists( 'ReduxFramework' ) ) {
                     return;
                 }
             
                 // This is needed. Bah WordPress bugs.  <img draggable="false" class="emoji" alt="😉" src="https://s.w.org/images/core/emoji/72x72/1f609.png">
                 if ( true == Redux_Helpers::isTheme( __FILE__ ) ) {
                     $this->initSettings();
                 } else {
                     add_action( 'plugins_loaded', array( $this, 'initSettings' ), 10 );
                 }
             
            }

            public function initSettings() {
 
                // Set the default arguments
                $this->setArguments();
             
                // Set a few help tabs so you can see how it's done
                $this->setHelpTabs();
             
                // Create the sections and fields
                $this->setSections();
             
                if ( ! isset( $this->args['opt_name'] ) ) { // No errors please
                    return;
                }
             
                $this->ReduxFramework = new ReduxFramework( $this->sections, $this->args );
            }

            /**
                Thiết lập cho method setAgruments
                Method này sẽ chứa các thiết lập cơ bản cho trang Options Framework như tên menu chẳng hạn
                **/
                public function setArguments() {
                    $theme = wp_get_theme(); // Lưu các đối tượng trả về bởi hàm wp_get_theme() vào biến $theme để làm một số việc tùy thích.
                    $this->args = array(
                            // Các thiết lập cho trang Options
                            'opt_name'  => 'tp_options', // Tên biến trả dữ liệu của từng options, ví dụ: tp_options['field_1']
                            'display_name' => $theme->get( 'Name' ), // Thiết lập tên theme hiển thị trong Theme Options
                            'menu_type'          => 'menu',
                            'allow_sub_menu'     => true,
                            'menu_title'         => __( 'TP Theme Options', 'thachpham' ),
                            'page_title'         => __( 'TP Theme Options', 'thachpham' ),
                            'dev_mode' => false,
                            'customizer' => true,
                            'menu_icon' => '', // Đường dẫn icon của menu option
                            // Chức năng Hint tạo dấu chấm hỏi ở mỗi option để hướng dẫn người dùng */
                            'hints'              => array(
                                'icon'          => 'icon-question-sign',
                                'icon_position' => 'right',
                                'icon_color'    => 'lightgray',
                                'icon_size'     => 'normal',
                                'tip_style'     => array(
                                    'color'   => 'light',
                                    'shadow'  => true,
                                    'rounded' => false,
                                    'style'   => '',
                                ),
                                'tip_position'  => array(
                                    'my' => 'top left',
                                    'at' => 'bottom right',
                                ),
                                'tip_effect'    => array(
                                    'show' => array(
                                        'effect'   => 'slide',
                                        'duration' => '500',
                                        'event'    => 'mouseover',
                                    ),
                                    'hide' => array(
                                        'effect'   => 'slide',
                                        'duration' => '500',
                                        'event'    => 'click mouseleave',
                                    ),
                                ),
                            ),
                            'google_api_key'       => 'AIzaSyAs0iVWrG4E_1bG244-z4HRKJSkg7JVrVQ',
                            // Set it you want google fonts to update weekly. A google_api_key value is required.
                            // end Hints
                    );

                    $this->args['share_icons'][] = array(
                        'url'   => 'https://www.facebook.com/pages/Redux-Framework/243141545850368',
                        'title' => 'Like us on Facebook',
                        'icon'  => 'el-icon-facebook'
                    );
                }


                    /**
                        Thiết lập khu vực Help để hướng dẫn người dùng
                    **/
                    public function setHelpTabs() {
                     
                        // Custom page help tabs, displayed using the help API. Tabs are shown in order of definition.
                        $this->args['help_tabs'][] = array(
                            'id'      => 'redux-help-tab-1',
                            'title'   => __( 'Theme Information 1', 'khangdoan' ),
                            'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'khangdoan' )
                        );
                     
                        $this->args['help_tabs'][] = array(
                            'id'      => 'redux-help-tab-2',
                            'title'   => __( 'Theme Information 2', 'khangdoan' ),
                            'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'khangdoan' )
                        );
                     
                        // Set the help sidebar
                        $this->args['help_sidebar'] = __( '<p>This is the sidebar content, HTML is allowed.</p>', 'khangdoan' );
                    }

                    /**
                        Thiết lập từng phần trong khu vực Theme Options
                        mỗi section được xem như là một phân vùng các tùy chọn
                        Trong mỗi section có thể sẽ chứa nhiều field
                    **/
                    public function setSections() {
                     
                        // logo Section
                        $this->sections[] = array(
                            'title'  => __( 'Header', 'khangdoan' ),
                            'desc'   => __( 'All of settings for header on this theme.', 'khangdoan' ),
                            'icon'   => 'el-icon-home',
                            'fields' => array(
                                array(
                                    'id'       => 'logo-on',
                                    'type'     => 'switch',
                                    'title'    => __( 'Enable Image Logo', 'khangdoan' ),
                                    'compiler' => 'bool', // Trả về giá trị kiểu true/false (boolean)
                                    'desc'     => __( 'Do you want to use image as a logo?', 'khangdoan' ),
                                    'on' => __( 'Enabled', 'khangdoan' ),
                                    'off' => __('Disabled', 'khangdoan')
                                ),
                             
                                array(
                                    'id'       => 'logo-image',
                                    'type'     => 'media',
                                    'title'    => __( 'Logo Image', 'khangdoan' ),
                                    'desc'     => __( 'Image that you want to use as logo', 'khangdoan' ),
                                ),
                            )
                        ); // end section


                        $this->sections[] = array(
                            'title'  => __( 'Typography', 'khangdoan' ),
                            'desc'   => __( 'All of settings for typography on this theme.', 'khangdoan' ),
                            'icon'   => 'el-icon-font',
                            'fields' => array(
                                array(
                                    'id'       => 'typo-main',
                                    'type'     => 'typography',
                                    'title'    => __( 'Main Typography', 'khangdoan' ),
                                    'output'   => 'body',
                                    'text-transform' => true,
                                    'default' => array(
                                        'font-size' => '14px',
                                        'font-family' => '"Helvetica Neue", Arial, sans-serif',
                                        'color' => '#333333',

                                    )
                                ),
                             
                            )
                        ); // end section


                        $this->sections[] = array(
                            'title'  => __( 'Slider', 'khangdoan' ),
                            'desc'   => __( 'Slider.', 'khangdoan' ),
                            'icon'   => 'el-icon-font',
                            'fields' => array(
                                array(
                                    'id'        => 'opt-multi-media',
                                    'type'      => 'multi_media',
                                    'title'     => 'Multi Media Selector',
                                    'subtitle'  => 'Multi file media selector',
                                    'labels'    => array(
                                        'upload_file'       => __('Select File(s)', 'redux-framework-demo'),
                                        'remove_image'      => __('Remove Image', 'redux-framework-demo'),
                                        'remove_file'       => __('Remove', 'redux-framework-demo'),
                                        'file'              => __('File: ', 'redux-framework-demo'),
                                        'download'          => __('Download', 'redux-framework-demo'),
                                        'title'             => __('Multi Media Selector', 'redux-framework-demo'),
                                        'button'            => __('Add or Upload File','redux-framework-demo')
                                    ),
                                    'library_filter'  => array('gif','jpg','png'),
                                    'max_file_upload' => 5
                                ),
                            )
                         );   
                    }
    }

        global $reduxConfigs;
        $reduxConfigs = new wp_theme1_options();
    }

?>
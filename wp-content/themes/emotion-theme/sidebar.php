<!-- BEGIN SIDEBAR -->
							<aside id="sidebar" class="sidebar grid_4">
								<!-- Popular Posts Widget -->
								<div class="popular-posts widget widget__sidebar">
									<h3 class="widget-title">Most Popular</h3>
									<div class="widget-content">
										<ul class="posts-list unstyled clearfix">
											<?php
												if(have_posts()){
													while (have_posts()) {
														the_post();
													
											?>
											<li>
												<figure class="featured-thumb">
													<a href="#"><?php echo '<img src="'.get_bloginfo('template_directory').'/images/samples/img70x70.jpg" alt="" class="post-img" width="70" height="70">'; ?>
													</a>
												</figure>
												<h4><a href="#"><?php the_title(); ?></a></h4>
												<p class="post-meta"><?php the_date(); ?> by <a href="#"><?php the_author(); ?></a></p>
											</li>
											<?php
												}
												}
											?>
										</ul>
									</div>
								</div>
								<!-- /Popular Posts Widget -->
								
								<!-- Ad Spots Widget -->
								<div class="ad-spots widget widget__sidebar">
									<h3 class="widget-title">Ad Spots</h3>
									<div class="widget-content">
										<ul class="ad-holder unstyled clearfix">
											<li><a href="#"><?php echo '<img src="'.get_bloginfo('template_directory').'/images/samples/banner125x125.jpg" alt="" class="post-img" width="125" height="125">'; ?>
													</a></li>
											<li><a href="#"><?php echo '<img src="'.get_bloginfo('template_directory').'/images/samples/banner125x125.jpg" alt="" class="post-img" width="125" height="125">'; ?></a></li>
										</ul>
									</div>
								</div>
								<!-- /Ad Spots Widget -->
							</aside>
							<!-- END SIDEBAR -->
						</div>
						
						<!-- BEGIN BOTTOM SIDEBAR -->
						<aside class="sidebar sidebar__bottom">
							<div class="clearfix">
								<div class="grid_12">
									<h2>Our Clients</h2>
									<!-- Elastislide Carousel -->
									<ul id="carousel" class="elastislide-list">
										<!-- <li><a href="#"><img src="images/samples/client-logo1.png" alt="Client Logo" /></a></li>
										<li><a href="#"><img src="images/samples/client-logo2.png" alt="Client Logo" /></a></li>
										<li><a href="#"><img src="images/samples/client-logo3.png" alt="Client Logo" /></a></li>
										<li><a href="#"><img src="images/samples/client-logo4.png" alt="Client Logo" /></a></li>
										<li><a href="#"><img src="images/samples/client-logo1.png" alt="Client Logo" /></a></li>
										<li><a href="#"><img src="images/samples/client-logo2.png" alt="Client Logo" /></a></li>
										<li><a href="#"><img src="images/samples/client-logo3.png" alt="Client Logo" /></a></li>
										<li><a href="#"><img src="images/samples/client-logo4.png" alt="Client Logo" /></a></li>
										<li><a href="#"><img src="images/samples/client-logo3.png" alt="Client Logo" /></a></li>
										<li><a href="#"><img src="images/samples/client-logo4.png" alt="Client Logo" /></a></li>
										<li><a href="#"><img src="images/samples/client-logo1.png" alt="Client Logo" /></a></li>
										<li><a href="#"><img src="images/samples/client-logo2.png" alt="Client Logo" /></a></li>
									</ul> -->
									<!-- End Elastislide Carousel -->
								</div>
							</div>
						</aside>
						<!-- END BOTTOM SIDEBAR -->
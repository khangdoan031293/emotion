<?php
	get_header();

?>

				<!-- BEGIN CONTENT WRAPPER -->
				<div id="content-wrapper" class="content-wrapper">
					
					<div class="container">
						
						<div class="clearfix">
							<div class="grid_12">
							</div>
						</div>
						
						<div class="hr hr-dashed"></div>
						
						<div class="clearfix">
							<!-- BEGIN CONTENT -->
							<section id="content" class="grid_8">

								<div class="latest-posts-holder">
									<div class="search-info">
										<?php 
											$search_query = new WP_Query('s='.$s.'&showpost=-1');
											$search_keyword = wp_specialchars($s, 1);
											$search_count = $search_query->post_count;
											printf(__('We found %1$s articles for your search query', 'khangdoan'), $search_count);
											echo '<div style="padding-bottom:30px;"></div>';
										?>
									</div>

									<?php
										if(have_posts()){
											while (have_posts()) {
												$post = get_post();
												the_post();
												
										
									?>
									<article class="post clearfix">
										<figure class="featured-thumb">
											<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('large'); ?></a>
										</figure>
										<div class="post-body">
											<header class="post-header">
												<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
												<p class="post-meta">
													<span class="post-meta-cats"><i class="icon-tag"></i><?php if(has_tag()){the_tags('', ' / ');} ?></span>
													<span class="post-meta-author"><a href="<?php the_author_link(); ?>"><i class="icon-user"></i><?php the_author(); ?></a></span>
													<span class="post-meta-comments"><a href="<?php comment_link(); ?>"><i class="icon-comment"></i><?php comments_popup_link( 
														__('0'),
														 __('1'), 
														 __('%') );  ?></a></span>
												</p>
											</header>
											<div class="post-excerpt">
												<p><?php
												if(!is_single()&&!is_page()){
													 the_excerpt(); 
												}else{
													the_content();
												}
												?></p>
											</div>
										</div>
									</article>
									<?php
											}	
										} else{
											get_template_part( 'content', 'none');
										}
									?>
								</div>

								<?php 
											// Navigation 
											emotion_pagging();
											//Navigation ?>
							</section>
							<!-- END CONTENT -->
							
							<?php get_sidebar(); ?>
						
						
					</div>
					
				</div>
				<!-- END CONTENT WRAPPER -->

<?php
	get_footer();	
?>
<aside id="sidebar" class="sidebar grid_4">
							
							<!-- Search Widget -->
							<div class="search-widget widget widget__sidebar">
								<div class="widget-content">
										<?php 
											get_search_form(true);
										?>
								</div>
							</div>
								
							<!-- /Search Widget -->
							
							<!-- Popular Posts Widget -->
							<div class="popular-posts widget widget__sidebar">
								<h3 class="widget-title">Most Popular</h3>
								<div class="widget-content">
									<ul class="posts-list unstyled clearfix">
										<?php
											if(have_posts()){
												while (have_posts()) {
													the_post();
												
										?>
										<li>
											<figure class="featured-thumb">
												<a href="#"><img src="<?php bloginfo('template_directory'); ?>/images/samples/img70x70.jpg" alt="" width="70" height="70"></a>
											</figure>
											<h4><a href="#"><?php the_title(); ?></a></h4>
											<p class="post-meta"><?php the_date() ?> by <a href="#"><?php the_author(); ?></a></p>
										</li>
										<?php
											}
											}
										?>
									</ul>
								</div>
							</div>
							<!-- /Popular Posts Widget -->
							
							<!-- Ad Spots Widget -->
							<div class="ad-spots widget widget__sidebar">
								<h3 class="widget-title">Ad Spots</h3>
								<div class="widget-content">
									<ul class="ad-holder unstyled clearfix">
										<li><a href="#"><img src="<?php bloginfo('template_directory'); ?>/images/samples/banner125x125.jpg" alt="" width="125" height="125"></a></li>
										<li><a href="#"><img src="<?php bloginfo('template_directory'); ?>/images/samples/banner125x125.jpg" alt="" width="125" height="125"></a></li>
										<li><a href="#"><img src="<?php bloginfo('template_directory'); ?>/images/samples/banner125x125.jpg" alt="" width="125" height="125"></a></li>
										<li><a href="#"><img src="<?php bloginfo('template_directory'); ?>/images/samples/banner125x125.jpg" alt="" width="125" height="125"></a></li>
									</ul>
								</div>
							</div>
							<!-- /Ad Spots Widget -->
							
							<!-- Text Widget -->
							<div class="text-widget widget widget__sidebar">
								<h3 class="widget-title"><?php echo get_the_title (134); ?></h3>
								<div class="widget-content">
									<?php echo get_post_field('post_content', 134); ?>
								</div>
							</div>
							<!-- /Text Widget -->
							
							
							<!-- Archives Widget -->
							<div class="archives-widget widget widget__sidebar">	

								<h3 class="widget-title">Archives</h3>
								<div class="widget-content">
									<ul>
										<?php wp_get_archives(); ?>
									</ul>
								</div>
							</div>
							<!-- /Archives Widget -->
							
						</aside>
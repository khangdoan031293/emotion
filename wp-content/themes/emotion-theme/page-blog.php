<?php 
	get_header();
?>


<!-- BEGIN PAGE TITLE -->
				<div id="page-title" class="page-title">
					<div class="container clearfix">
						<div class="grid_12">
							<div class="page-title-holder clearfix">
								<h1><?php wp_title( '', true, '' ); ?></h1>
							</div>
						</div>
					</div>
				</div>
				<!-- END PAGE TITLE -->
				
				
				<!-- BEGIN CONTENT WRAPPER -->
				<div id="content-wrapper" class="content-wrapper">
					<div class="container clearfix">
						<!-- Content -->
						<div id="content" class="grid_8">
							<!-- Post Standard -->
							<article class="entry entry__standard clearfix">
								<!-- begin post image -->
								<figure class="featured-thumb">
									<a href="#"><img src="<?php bloginfo('template_directory'); ?>/images/samples/img620x258.jpg" alt="" width="620" height="258" border="0" /></a>
								</figure>
								<!-- end post image -->
								
								<!-- begin post heading -->
								<header class="entry-header clearfix">
									<div class="format-icon">
										<i class="icon-file-alt"></i>
									</div>
									<div class="entry-header-inner">
										<h2 class="entry-title"><a href="#">This is a Standard Post Format</a></h2>
										<p class="post-meta">
											<span class="post-meta-cats"><i class="icon-tag"></i><a href="#">News</a> / <a href="#">Design</a></span>
											<span class="post-meta-author"><a href="#"><i class="icon-user"></i>Dan Fisher</a></span>
											<span class="post-meta-comments"><a href="#"><i class="icon-comment"></i>16</a></span>
										</p>
									</div>
								</header>
								<!-- end post heading -->
								
								<!-- begin post content -->
								<div class="entry-content">
									<div>Fusce ac pharetra urna. Duis non lacus sit amet lacus interdum facilisis sed non est. Ut mi metus, semper eu dictum nec, condimentum sed sapien. Nullam lobortis nunc semper ipsum luctus ut viverra ante eleifend. Nunc pretium velit sed augue luctus accumsan.</div>
								</div>
								<!-- end post content -->
								
								<!-- begin post footer -->
								<footer class="entry-footer">
									<a href="#" class="btn">Read More</a>
								</footer>
								<!-- end post footer -->
							</article>
							<!-- /Post Standard -->
							
							
							<!-- Post Gallery -->
							<article class="entry entry__gallery clearfix">
								<!-- begin post image -->
								<div class="flexslider slide">
								  <ul class="slides">
								    <li>
								      <img src="<?php bloginfo('template_directory'); ?>/images/samples/img620x258.jpg" alt="" width="620" height="258" border="0" />
								    </li>
								    <li>
								      <img src="<?php bloginfo('template_directory'); ?>/images/samples/img620x258.jpg" alt="" width="620" height="258" border="0" />
								    </li>
								    <li>
								      <img src="<?php bloginfo('template_directory'); ?>/images/samples/img620x258.jpg" alt="" width="620" height="258" border="0" />
								    </li>
								  </ul>
								</div>
								<!-- end post image -->
								
								<!-- begin post heading -->
								<header class="entry-header clearfix">
									<div class="format-icon">
										<i class="icon-picture"></i>
									</div>
									<div class="entry-header-inner">
										<h2 class="entry-title"><a href="#">This is a Gallery Post Format</a></h2>
										<p class="post-meta">
											<span class="post-meta-cats"><i class="icon-tag"></i><a href="#">News</a> / <a href="#">Design</a></span>
											<span class="post-meta-author"><a href="#"><i class="icon-user"></i>Dan Fisher</a></span>
											<span class="post-meta-comments"><a href="#"><i class="icon-comment"></i>16</a></span>
										</p>
									</div>
								</header>
								<!-- end post heading -->
								
								<!-- begin post content -->
								<div class="entry-content">
									<div>Fusce ac pharetra urna. Duis non lacus sit amet lacus interdum facilisis sed non est. Ut mi metus, semper eu dictum nec, condimentum sed sapien. Nullam lobortis nunc semper ipsum luctus ut viverra ante eleifend. Nunc pretium velit sed augue luctus accumsan.</div>
								</div>
								<!-- end post content -->
								
								<!-- begin post footer -->
								<footer class="entry-footer">
									<a href="#" class="btn">Read More</a>
								</footer>
								<!-- end post footer -->
							</article>
							<!-- /Post Gallery -->
							
							
							<!-- Post Video -->
							<article class="entry entry__video clearfix">
								<!-- begin video -->
								<figure class="video">
									<iframe src="http://player.vimeo.com/video/7449107?title=0&byline=0&portrait=0" width="530" height="298" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
								</figure>
								<!-- end video -->
								
								<!-- begin post heading -->
								<header class="entry-header clearfix">
									<div class="format-icon">
										<i class="icon-film"></i>
									</div>
									<div class="entry-header-inner">
										<h2 class="entry-title"><a href="#">This is a Video Post Format</a></h2>
										<p class="post-meta">
											<span class="post-meta-cats"><i class="icon-tag"></i><a href="#">News</a> / <a href="#">Design</a></span>
											<span class="post-meta-author"><a href="#"><i class="icon-user"></i>Dan Fisher</a></span>
											<span class="post-meta-comments"><a href="#"><i class="icon-comment"></i>16</a></span>
										</p>
									</div>
								</header>
								<!-- end post heading -->
								
								<!-- begin post content -->
								<div class="entry-content">
									<div>Fusce ac pharetra urna. Duis non lacus sit amet lacus interdum facilisis sed non est. Ut mi metus, semper eu dictum nec, condimentum sed sapien. Nullam lobortis nunc semper ipsum luctus ut viverra ante eleifend. Nunc pretium velit sed augue luctus accumsan.</div>
								</div>
								<!-- end post content -->
								
								<!-- begin post footer -->
								<footer class="entry-footer">
									<a href="#" class="btn">Read More</a>
								</footer>
								<!-- end post footer -->
							</article>
							<!-- /Post Video -->
							
							
							<!-- Post Quote -->
							<article class="entry entry__quote clearfix">
								<!-- begin post heading -->
								<header class="entry-header">
									<div class="format-icon">
										<i class="icon-quote-left"></i>
									</div>
								</header>
								<!-- end post heading -->
								<!-- begin post content -->
								<div class="entry-content">
									<!-- Blockquote -->
									<blockquote>
									<p>Lorem ipsum dolor sit amet, consecte adipiscing elit. Integer aliquam mi nec dolor placerat a condimentum diam mollis. Ut pulvinar neque eget massa dapibus dolor interdum. Phasellus a massa et.</p>
									<cite>- John Doe</cite>
									</blockquote>
									<!-- /Blockquote -->
								</div>
								<!-- end post content -->
							</article>
							<!-- /Post Quote -->
							
							
							<!-- Post Link -->
							<article class="entry entry__link clearfix">
								<!-- begin post heading -->
								<header class="entry-header clearfix">
									<div class="format-icon">
										<i class="icon-link"></i>
									</div>
									<div class="entry-header-inner">
										<h2 class="entry-title"><a href="#">[Link] This is a Link Post Format</a></h2>
									</div>
								</header>
								<!-- end post heading -->
								<!-- begin post content -->
								<div class="entry-content">
									Fusce ac pharetra urna. Duis non lacus sit amet lacus interdum facilisis sed non est. Ut mi metus, semper eu dictum nec, condimentum sed sapien. Nullam lobortis nunc semper ipsum luctus ut viverra ante eleifend. Nunc pretium velit sed augue luctus accumsan.
								</div>
								<!-- end post content -->
							</article>
							<!-- /Post Link -->
							
							
							<!-- Post Standard -->
							<article class="entry entry__standard clearfix">
								
								<!-- begin post heading -->
								<header class="entry-header clearfix">
									<div class="format-icon">
										<i class="icon-file-alt"></i>
									</div>
									<div class="entry-header-inner">
										<h2 class="entry-title"><a href="#">This is a Standard Post Format without Image</a></h2>
										<p class="post-meta">
											<span class="post-meta-cats"><i class="icon-tag"></i><a href="#">News</a> / <a href="#">Design</a></span>
											<span class="post-meta-author"><a href="#"><i class="icon-user"></i>Dan Fisher</a></span>
											<span class="post-meta-comments"><a href="#"><i class="icon-comment"></i>16</a></span>
										</p>
									</div>
								</header>
								<!-- end post heading -->
								
								<!-- begin post content -->
								<div class="entry-content">
									<div>Fusce ac pharetra urna. Duis non lacus sit amet lacus interdum facilisis sed non est. Ut mi metus, semper eu dictum nec, condimentum sed sapien. Nullam lobortis nunc semper ipsum luctus ut viverra ante eleifend. Nunc pretium velit sed augue luctus accumsan.</div>
								</div>
								<!-- end post content -->
								
								<!-- begin post footer -->
								<footer class="entry-footer">
									<a href="#" class="btn">Read More</a>
								</footer>
								<!-- end post footer -->
							</article>
							<!-- /Post Standard -->
							
							
							<!-- BEGIN PAGINATION -->
							<ul class="pagination">
								<li class="prev"><a href="#">&larr;</a></li>
								<li><span class="current">1</span></li>
								<li><a href="#">2</a></li>
								<li><a href="#">3</a></li>
								<li><a href="#">4</a></li>
								<li><span class="gap">...</span></li>
								<li><a href="#">8</a></li>
								<li><a href="#">9</a></li>
								<li class="next"><a href="#">&rarr;</a></li>
							</ul>
							<!-- END PAGINATION -->
							
						</div>
						<!-- /Content -->
						
						<!-- Sidebar -->
							<?php get_sidebar('blog'); ?>
						<!-- /Sidebar -->
					</div>
				</div>
				<!-- END CONTENT WRAPPER -->

			
<?php
	get_footer();
?>
<?php

/**
@ Declare constant values
	@ THEME_URL = get path theme folder
	@ CORE = get path 'core' folder
**/

define( 'THEME_URL', get_stylesheet_directory());
define( 'CORE', THEME_URL.'/core');

/**
@embed core/init.php file
**/

require_once(CORE.'/init.php');

/**
@ Decalre function Theme
**/

if(!function_exists('emotion_theme_setup')){
	function emotion_theme_setup(){
		/*
			Add RSS link <head> Automatically
		*/

		add_theme_support( 'Automatic_feed_links' );

		/*
			Add post Thumbnail
		*/

		add_theme_support( 'post-thumbnails' );

		/*
			Post format
		*/

		add_theme_support( 'post-formats', array(
				'image',
				'video',
				'gallery',
				'quote',
				'link'
			) );

		/*
			Add title-tag
		*/

		add_theme_support( 'title-tag' );

		/*
			Add custom background
		*/

		$default_background = array('default-color' => '#e8e8e8');
		add_theme_support( 'custom-background', $default_background );

		/* Add menu */

		register_nav_menu( 'main-menu', __('Main Menu', 'khangdoan') );

		/*
			Create sidebar
		*/

		$sidebar = array(
			'name' => __('Main Sidebar', 'khangdoan'),
			'id' => 'main-sidebar',
			'description' => __('Default sidebar'),
			'class' => 'main-sidebar',
			'before_title' => '<h3 class="widgettitle">',
			'after_title' => '</h3>'
		);

		register_sidebar( $sidebar );
	}
}

add_action('init', 'emotion_theme_setup');

/**
	Header function
**/


if(!function_exists('header_logo')){
	function header_logo(){
		global $tp_options;

		if($tp_options['logo-on'] == 1){
			printf('<a href="%1$s" title="%2$s"><img src="'.$tp_options['logo-image']['url'].'" alt="Emotion" width="145" height="67" ></a>',
				get_bloginfo('url' ),
				get_bloginfo('description' )					
			);
		}
	}
}


class Menu_With_Description extends Walker_Nav_Menu {
	function start_el(&$output, $item, $depth, $args) {
		global $wp_query;
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
		
		$class_names = $value = '';

		$classes = empty( $item->classes ) ? array() : (array) $item->classes;

		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
		$class_names = ' class="' . esc_attr( $class_names ) . '"';

		$output .= $indent . '<li id="menu-item-'. $item->ID . '"' . $value . $class_names .'>';

		$attributes = ! empty( $item->attr_title ) ? ' title="' . esc_attr( $item->attr_title ) .'"' : '';
		$attributes .= ! empty( $item->target ) ? ' target="' . esc_attr( $item->target ) .'"' : '';
		$attributes .= ! empty( $item->xfn ) ? ' rel="' . esc_attr( $item->xfn ) .'"' : '';
		$attributes .= ! empty( $item->url ) ? ' href="' . esc_attr( $item->url ) .'"' : '';

		$item_output = $args->before;
		$item_output .= '<a'. $attributes .'>';
		$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
		$item_output .= '<br /><span class="sub">' . $item->description . '</span>';
		$item_output .= '</a>';
		$item_output .= $args->after;

		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}
}



if(!function_exists('header_menu')){
	function header_menu($menu){
		$menu = array(
				'theme_location' => 'main-menu',
				'container' => 'nav',
				'container_class' => $menu,
				'items_wrap' => '<ul id="%1$s" class="%2$s sf-menu">%3$s</ul>',
				'walker' => new Menu_With_Description()
			);
		wp_nav_menu($menu);
	}
}

/**
 * Show Thumbnail
 */
if(!function_exists('emotion_thumbnail')){
	function emotion_thumbnail($size){
		if(!is_single() && has_post_thumbnail() && !post_password_required() || has_post_format('image')){
			?>
				<figure class="post-thumbnail">
				<?php the_post_thumbnail($size); ?>
				</figure>
			<?php
		}
	}
}

/**
 * Show title post
 */

if(!function_exists('emotion_thumbnail_entry_header')){
	function emotion_thumbnail_entry_header(){
		if(is_single()){
			?>
				<h1 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php the_title();?>"><?php the_title(); ?></a></h1>
			<?php
		} else {
			?>
				<h3 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php the_title();?>"><?php the_title(); ?></a></h3>
			<?php
		}
		
	}
}

/**
 * Show author post
 */

if(!function_exists('emotion_thumbnail_entry_meta')){
	function emotion_thumbnail_entry_meta(){
		if(!is_page()){
			?>
				<div class="entry-meta">
					<?php 
						printf(__('<span class="post-meta-author"><a href="">%1$s<i class="icon-user"></i></a></span>', 'khangdoan'), get_the_author());

						printf(__('<span class="post-meta-comments"><a href="">%1$s<i class="icon-comment"></i></a></span>', 'khangdoan'), comments_number('0', '1', '%'));


						// if(comments_open()){
						// 	echo '<span class="meta-reply">';
						// 		comments_popup_link(
						// 			__('Leave a comment', 'khangdoan'),
						// 			__('One comment', 'khangdoan'),
						// 			__('% comments'),
						// 			__('Read all comments', 'khangdoan')
						// 			);
						// 	echo '</span>';
						// }
					?>
				</div>
			<?php
		}
	}
}

/**
 * Show content post
 */

if(!function_exists('emotion_thumbnail_entry_content')){
	function emotion_thumbnail_entry_content(){
		if(!is_single() && !is_page()){
			the_excerpt();
		} else{
			
			the_content();
			/* Pagging single	*/

			$link_pages = array(
				'before' => __('<p>Page', 'khangdoan'),
				'after' => __('</p>'),
				'nextpagelink' => __('Next Page', 'khangdoan'),
				'previouspagelink' => __('Previous Page', 'khangdoan')
				);
			wp_link_pages($link_pages);
		}
	}
}

/**
 * Show tag post
 */

if(!function_exists('emotion_thumbnail_entry_tag')){
	function emotion_thumbnail_entry_tag(){
		if( has_tag()){
			echo '<div class="entry-tag">';
			printf(__('<span class="post-meta-cats"><a href=""><i class="icon-tag"></i>%1$s</a></span>', 'khangdoan'), get_the_tag_list(' ', '/'));
			echo '</div>';
		}
	}
}


/* Simple paggination */

if(!function_exists('emotion_pagging')){
	function emotion_pagging(){
		if($GLOBALS['wp_query']->max_num_pages < 2){
			return '';
		}?>
			<nav class="pagination post-nav" role="navigation">
				<?php
					 
					if(get_previous_posts_link( )){
					?>
						<div class="next newer fright">
							<?php previous_posts_link(__('Newer Entries &rarr;', 'khangdoan')); ?>
						</div>
					<?php
					}

					if(get_next_posts_link()){
					?>
						<div class="prev older fleft">
							<?php next_posts_link(__('&larr; Older Entries', 'khangdoan')); ?>
						</div>
					<?php
					}
				?>
			</nav>
		<?php
	}
}

// Read more

function emotion_readmore(){
	return '<br><a class="btn" href="'.get_permalink(get_the_ID()).'">'.__('Read More', 'khangdoan').'</a>';
}

add_filter('excerpt_more', 'emotion_readmore');

/**
 * Filter the except length to 20 characters.
 *
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
 */
function wpdocs_custom_excerpt_length( $length ) {
    return 20;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 1 );


/**
	Embed styles and js theme
**/

function emotion_theme_style(){

	//Css
	wp_register_style('main-style', get_template_directory_uri()."/style.css", "all");
	wp_enqueue_style('main-style');

	wp_register_style('base-style', get_template_directory_uri()."/css/base.css", "all");
	wp_enqueue_style('base-style');

	wp_register_style('flexslider-style', get_template_directory_uri()."/css/flexslider.css", "all");
	wp_enqueue_style('flexslider-style');

	wp_register_style('fontawesome-style', get_template_directory_uri()."/css/font-awesome.min.css", "all");
	wp_enqueue_style('fontawesome-style');

	wp_register_style('mediaqueries-style', get_template_directory_uri()."/css/media-queries.css", "all");
	wp_enqueue_style('mediaqueries-style');

	wp_register_style('normalize-style', get_template_directory_uri()."/css/normalize.css", "all");
	wp_enqueue_style('normalize-style');

	wp_register_style('prettyPhoto-style', get_template_directory_uri()."/css/prettyPhoto.css", "all");
	wp_enqueue_style('prettyPhoto-style');

	wp_register_style('settings-style', get_template_directory_uri()."/css/settings.css", "all");
	wp_enqueue_style('settings-style');

	wp_register_style('skeleton-style', get_template_directory_uri()."/css/skeleton.css", "all");
	wp_enqueue_style('skeleton-style');

	wp_register_style('superfish-style', get_template_directory_uri()."/css/superfish.css", "all");
	wp_enqueue_style('superfish-style');

	//js

	wp_register_script('jflickrfeed-script', get_template_directory_uri()."/js/jflickrfeed.js", array('jquery'));
	wp_enqueue_script('jflickrfeed-script');

	wp_register_script('easing-script', get_template_directory_uri()."/js/jquery.easing.min.js", array('jquery'));
	wp_enqueue_script('easing-script');

	wp_register_script('elastislide-script', get_template_directory_uri()."/js/jquery.elastislide.js", array('jquery'));
	wp_enqueue_script('elastislide-script');

	wp_register_script('flexslider-script', get_template_directory_uri()."/js/jquery.flexslider.js", array('jquery'));
	wp_enqueue_script('flexslider-script');

	wp_register_script('form-script', get_template_directory_uri()."/js/jquery.form.js", array('jquery'));
	wp_enqueue_script('form-script');

	wp_register_script('imagesloaded-script', get_template_directory_uri()."/js/jquery.imagesloaded.min.js", array('jquery'));
	wp_enqueue_script('imagesloaded-script');

	wp_register_script('isotope-script', get_template_directory_uri()."/js/jquery.isotope.min.js", array('jquery'));
	wp_enqueue_script('isotope-script');

	wp_register_script('mobilemenu-script', get_template_directory_uri()."/js/jquery.mobilemenu.js", array('jquery'));
	wp_enqueue_script('mobilemenu-script');

	wp_register_script('prettyPhoto-script', get_template_directory_uri()."/js/jquery.prettyPhoto.js", array('jquery'));
	wp_enqueue_script('prettyPhoto-script');

	wp_register_script('superfish-script', get_template_directory_uri()."/js/jquery.superfish-1.5.0.js", array('jquery'));
	wp_enqueue_script('superfish-script');

	wp_register_script('themepunch-script', get_template_directory_uri()."/js/jquery.themepunch.plugins.min.js", array('jquery'));
	wp_enqueue_script('themepunch-script');

	wp_register_script('themepunchrevolution-script', get_template_directory_uri()."/js/jquery.themepunch.revolution.min.js", array('jquery'));
	wp_enqueue_script('themepunchrevolution-script');

	// wp_register_script('twitter-script', get_template_directory_uri()."/js/jquery.twitter.js", array('jquery'));
	// wp_enqueue_script('twitter-script');

	// wp_register_script('map-script', get_template_directory_uri()."/js/jquery.ui.map.js", array('jquery'));
	// wp_enqueue_script('map-script');

	wp_register_script('migrate-script', get_template_directory_uri()."/js/jquery-migrate-1.1.1.min.js", array('jquery'));
	wp_enqueue_script('migrate-script');

	wp_register_script('modernizr-script', get_template_directory_uri()."/js/modernizr.custom.17475.js", array('jquery'));
	wp_enqueue_script('modernizr-script');

	wp_register_script('custom-script', get_template_directory_uri()."/custom.js", array('jquery'));
	wp_enqueue_script('custom-script');
}


add_action('wp_enqueue_scripts', 'emotion_theme_style');
?>
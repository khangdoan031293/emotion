

<?php
	get_header();

?>

				<!-- BEGIN CONTENT WRAPPER -->
				<div id="content-wrapper" class="content-wrapper">
					
					<div class="container">
						
						<div class="clearfix">
							<div class="grid_12">
							</div>
						</div>
						
						<div class="hr hr-dashed"></div>
						
						<div class="clearfix">
							<!-- BEGIN CONTENT -->
							<section id="content" class="grid_8">

								<div class="latest-posts-holder">
									<?php
										if(have_posts()){
											while (have_posts()) {
												$post = get_post();
												the_post();
												
										
									?>
									<article class="post clearfix">
										<figure class="featured-thumb">
											<?php the_post_thumbnail('small'); ?>
										</figure>
										<div class="post-body">
											<header class="post-header">
												<h1><?php the_title(); ?></h1>
												<p class="post-meta">
													<span class="post-meta-cats"><i class="icon-tag"></i><?php if(has_tag()){the_tags('', ' / ');} ?></span>
													<span class="post-meta-author"><a href="<?php the_author_link(); ?>"><i class="icon-user"></i><?php the_author(); ?></a></span>
													<span class="post-meta-comments"><a href="<?php comment_link(); ?>"><i class="icon-comment"></i><?php comments_popup_link( 
														__('0'),
														 __('1'), 
														 __('%') );  ?></a></span>
												</p>
											</header>
											<div class="post-excerpt">
												<p><?php
												
													the_content();
													echo '<div style="clear:both; padding-top: 50px;"></div>';
													comments_template();
												?></p>
											</div>
										</div>
									</article>
									<?php
											}	
										} else{
											get_template_part( 'content', 'none');
										}
									?>
								</div>

								<?php 
											// Navigation 
											emotion_pagging();
											//Navigation ?>
							</section>
							<!-- END CONTENT -->
							
							<?php get_sidebar(); ?>
						
						
					</div>
					
				</div>
				<!-- END CONTENT WRAPPER -->

<?php
	get_footer();	
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>" />
	<link rel="profile"  href="http://gmgp.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
	
	<!-- Favicons
	================================================== -->
	<link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/images/favicon.ico">
	<link rel="apple-touch-icon" href="<?php bloginfo('template_directory'); ?>/images/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php bloginfo('template_directory'); ?>/images/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php bloginfo('template_directory'); ?>/images/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php bloginfo('template_directory'); ?>/images/apple-touch-icon-144x144.png">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?> >
		<!-- BEGIN WRAPPER -->
	<div id="wrapper">
			
		<div class="wrapp-holder">
			<div class="wrap-frame">
				<!-- BEGIN HEADER -->
				<header id="header" class="header">
				
					<!-- Top Header -->
					<div class="header-top">
						<div class="container clearfix">
							<div class="grid_12">
								<!-- Top Menu -->
								<ul class="header-top-menu unstyled">
									<li><a href="#">Pricing</a></li>
									<li><a href="#">Sitemap</a></li>
									<li><a href="#">FAQs</a></li>
								</ul>
								<!-- /Top Menu -->
								
								<!-- Social Links -->
								<ul class="social-links unstyled">
									<li class="ico-twitter"><a href="#" style="background: url('<?php bloginfo("template_directory"); ?>/images/soc-icons.png') 0 0 no-repeat;">Twitter</a></li>
									<li class="ico-facebook"><a href="#" style="background: url('<?php bloginfo("template_directory"); ?>/images/soc-icons.png') 0 0 no-repeat;">Facebook</a></li>
									<li class="ico-googleplus"><a href="#" style="background: url('<?php bloginfo("template_directory"); ?>/images/soc-icons.png') 0 0 no-repeat;">Google+</a></li>
									<li class="ico-dribbble"><a href="#" style="background: url('<?php bloginfo("template_directory"); ?>/images/soc-icons.png') 0 0 no-repeat;">Dribbble</a></li>
									<li class="ico-vimeo"><a href="#" style="background: url('<?php bloginfo("template_directory"); ?>/images/soc-icons.png') 0 0 no-repeat;">Vimeo</a></li>
									<li class="ico-rss"><a href="#" style="background: url('<?php bloginfo("template_directory"); ?>/images/soc-icons.png') 0 0 no-repeat;">RSS</a></li>
								</ul>
								<!-- /Social Links -->
							</div>
						</div>
					</div>
					<!-- /Top Header -->
					
					<!-- Main Header -->
					<div class="header-main">
						<div class="container clearfix">
							<div class="grid_12 hr-bottom">
								
								<!-- BEGIN LOGO -->
								<div id="logo">
									<!-- Image based Logo -->
									<?php header_logo(); ?>
									
									<!-- Text based Logo
									<h1><a href="index.html">Emotion</a></h1>
									<p class="tagline">Responsive HTML Template</p>
									-->
									
								</div>
								<!-- END LOGO -->
								
								
								<!-- BEGIN NAVIGATION -->
								<?php header_menu('primary');?>
								<!-- END NAVIGATION -->
								
							</div>
						</div>
					</div>
					<!-- /Main Header -->
					
				</header>
				<!-- END HEADER -->
</div>
</div>

<!-- BEGIN FOOTER -->
		<footer id="footer" class="footer">
			
			<div class="footer-holder">
				<div class="footer-frame">
					<!-- Footer Widgets -->
					<div class="footer-widgets">
						<div class="container clearfix">
							
							<div class="grid_5">
								<!-- Text Widget -->
								<div class="text-widget widget widget__footer">
									<h3 class="widget-title"><?php echo get_the_title (97); ?></h3>
									<div class="widget-content">
										<p><?php echo get_post_field('post_content', 97); ?></p>
									</div>
								</div>
								<!-- /Text Widget -->
							</div>
							<div class="grid_3">
								<!-- Twitter Widget -->
								<div class="twitter-widget widget widget__footer">
									<h3 class="widget-title">Twitter</h3>
									<div class="widget-content">
										<ul id="twitter-feed" class="twitter_update_list"></ul>
									</div>
								</div>
								<!-- /Twitter Widget -->
							</div>
							<div class="grid_4">
								<!-- Flickr Widget -->
								<div class="flickr-widget widget widget__footer">
									<h3 class="widget-title">Flickr Feed</h3>
									<div class="widget-content">
										<!-- Flickr images will appear here -->
										<ul id="flickr" class="thumbs"></ul>
									</div>
								</div>
								<!-- /Flickr Widget -->
							</div>
							
						</div>
					</div>
					<!-- /Footer Widgets -->
					
					<!-- Copyright -->
					<div class="copyright">
						<div class="container clearfix">
							<div class="grid_12">
								<div class="clearfix">
									<div class="copyright-primary">
										&copy;  2013 Emotion <span class="separator">|</span> Responsive HTML Template
									</div>
									<div class="copyright-secondary">
										Created by <a href="http://themeforest.net/user/dan_fisher">Dan Fisher</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /Copyright -->
				</div>
			</div>
			
		</footer>
		<!-- END FOOTER -->
		
</div>

<?php wp_footer(); ?>
</body>
</html>